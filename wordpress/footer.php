        <footer>
            <div class="pre-footer">
                <div class="row">
                    <div class="medium-4 columns text-center margin-bottom-for-small">
                        <img src="<?php echo get_template_directory_uri() ?>/library/images/Activate.png" alt="Activate Clubs" />
                    </div> 
                    <div class="medium-4 columns text-center margin-bottom-for-small">
                        &copy; BIG 2015-2016. Authorised by the President of BiG. UTS BiG is an affiliated club of ActivateClubs.
                    </div>  
                    <div class="medium-4 columns text-center">
                        <b>CONTACT US</b><br>
                        info@utsbig.com.au
                    </div>  
                </div>
            </div>
            <div class="row" >
                <div class="medium-4 columns text-center">
                    <h5>
                        <a class="white" href="https://facebook.com/UTS.BIG" title="UTS.BIG" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp; 
                        <a class="white" href="https://twitter.com/utsbig" title="@utsbig" target="_blank"><i class="fa fa-twitter"></i></a> &nbsp; 
                        <a class="white" href="https://instagram.com/utsbig/" title="@utsbig" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp; 
                        <a class="white" title="utsbig" target="_blank" ><img src="<?php echo get_template_directory_uri() ?>/library/images/snapchat.png" class="snapchat" /></a> &nbsp; 
                    </h5>
                </div> 
                <div class="medium-4 columns text-center">
                    <img src="<?php echo get_template_directory_uri() ?>/library/images/big.png" alt="UTS BiG" />
                </div>  
                <div class="medium-4 columns text-center white">
                    Designed by Zeenia Kaur <br>
                    Developed by Paul Pagnan
                </div>  
            </div>
        </footer>  

        <div id="uri" style='display:none'><?php echo get_template_directory_uri() ?></div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
