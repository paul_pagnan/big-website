<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
 */

get_header(); ?>

	
	<?php big_menu(); ?>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'single' ); ?>


	<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>