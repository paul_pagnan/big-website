// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation({
    orbit: {
        timer_speed: 7000, // Sets the amount of time in milliseconds before transitioning a slide
        pause_on_hover: false, // Pauses on the current slide while hovering
        resume_on_mouseout: false, // If pause on hover is set to true, this setting resumes playback after mousing out of slide
        next_on_click: false, // Advance to next slide on click
    }
});

var uri = $("#uri").text();

if( !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
    var s = skrollr.init({
        smoothScrolling: true,
        forceHeight: false
    });

    skrollr.menu.init(s, {
        animate: true,
        easing: 'swing',
        duration: 900,
        updateUrl: false
    });

    if(is_home()) 
        window.sr = new scrollReveal();
}


//Define Counter vars
if(is_home()) {
    var members = new countUp("members", 0, 500);
    var events = new countUp("events", 0, 119);
    var execs = new countUp("execs", 0, 14);
}

$(document).ready(function() {

    
    if($("#wpadminbar")[0] !== undefined) {
        $(".fixed").attr('style', 'margin-top: 30px;');
    }
    

    if(isScrolledIntoView("#members") && is_home()) {
        members.start();   
        events.start();   
        execs.start();  
    }
   
    
    //Handle Social Tabs on load
    $(".social .tabs a").on('click', function(e) {
        e.preventDefault();
        var target = e.target;
        tabs($(target).parent().attr("href"));
    });
    
    
    if(is_home()) {
        loadTweets();
        
        //Define Instafeed vars
        var utsbig = new Instafeed({
            target: 'utsbig',
            get: 'tagged',
            tagName: 'utsbig',
            clientId: 'ecb0248c3f684a13882f161779855c4a',
            limit: 20
        });
        var bigcruiseroyale = new Instafeed({
            target: 'bigcruiseroyale',
            get: 'tagged',
            tagName: 'bigcruiseroyale',
            clientId: 'ecb0248c3f684a13882f161779855c4a',
            limit: 20
        });
        bigcruiseroyale.run();
        utsbig.run();
    }
});

var prevClick = 0;
var darudeMode = false;

$("#1,#2,#3,#4").click(function(e) {
    var target = e.target;
    var num = parseInt($(target).attr("id"));
    if(!darudeMode) {
        if(num == prevClick + 1)
            prevClick = num;
        else
            prevClick = 0;

        if(prevClick == 4) {
            darudeMode = true;
            for(var i = 0; i < 4; i++) {
                audio = document.createElement('audio');
                audio.id = "audio-" + (i + 1);
                source = document.createElement('source');
                source.src = uri + "/library/audio/" + (i + 1) + ".mp3";
                source.setAttribute('type','audio/mpeg');
                audio.appendChild(source);
                $("body").append(audio);
            }
        }
    }
    else {
        var fragment = document.getElementById("audio-" + num);
        fragment.play();
    }
});

function isSocial($target) {
    var skills = [ "instafeed", "twitter" ];
    for (var i = 0; i < skills.length; i++) {
        if($target.attr("id") == skills[i])
            return true;
    }   
    return false;
}

function tabs(id) {
    $(".social .tabs a").removeClass("white");
    $(id + "-button").addClass("white");
    if(id == "#twitter")
        $(".social").addClass("blue")
    else
        $(".social").removeClass("blue")

    $("#instafeed, #twitter").hide();
    $(id).fadeIn('slow');        
}


$(window).scroll(function() {
    if(isScrolledIntoView("#members")) {
        members.start();   
        events.start();   
        execs.start();   
    }
});



// http://stackoverflow.com/questions/9979827/change-active-menu-item-on-page-scroll
// Thanks to Marcus Ekwall
//var topMenu = $(".top-bar"),
//    topMenuHeight = topMenu.outerHeight()+15,
//    // All list items
//    menuItems = topMenu.find("a"),
//    // Anchors corresponding to menu items
//    scrollItems = menuItems.map(function(){
//        var item = $($(this).attr("href"));
//        if (item.length) { return item; }
//    });
//
//// Bind to scroll
//$(window).scroll(function(){
//    // Get container scroll position
//    var fromTop = $(this).scrollTop()+topMenuHeight;
//    // Get id of current scroll item
//    var cur = scrollItems.map(function(){
//        if ($(this).offset().top < fromTop)
//            return this;
//    });
//    // Get the id of the current element
//    cur = cur[cur.length-1];
//    var id = cur && cur.length ? cur[0].id : "";
//    
//    //Check if we are at the bottom and overide contact highlight
//    if($(window).scrollTop() + $(window).height() == $(document).height())
//        id="contact";
//    
//    // Set/remove active class
//    menuItems.parent().removeClass("active")
//    .end().filter("[href=#"+id+"]").parent().addClass("active");    
//});



//http://stackoverflow.com/questions/487073/check-if-element-is-visible-after-scrolling
//Thanks to Scott Dowding
function isScrolledIntoView(elem)
{
    try {
        var $elem = $(elem);
        var $window = $(window);

        var docViewTop = $window.scrollTop();
        var docViewBottom = docViewTop + $window.height();

        var elemTop = $elem.offset().top;
        var elemBottom = elemTop + $elem.height();
        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }
    catch(err) {
        return false;   
    }
    
}


function loadTweets() {
    $.getJSON(uri + '/library/getTweets.php', function(data) {
        var tweets = [];

        var wrap = "<div>%</div>";
        var body = "<p>%</p>";
        var date = "Posted %";
        var links = "<a href='https://twitter.com/intent/tweet?in_reply_to=%' class='twitter_reply_icon' target='_blank'><i class='fa fa-reply'></i></a><a href='https://twitter.com/intent/retweet?tweet_id=%' class='twitter_retweet_icon' target='_blank'><i class='fa fa-retweet'></i></a><a href='https://twitter.com/intent/favorite?tweet_id=%' class='twitter_fav_icon' target='_blank'><i class='fa fa-star'></i></a>";

        $.each( data, function() {
            var item1 = "";
            var item2 = "";
            var item3 = "";
            $.each( this, function( key, val ) {
                switch(key) {
                    case "text":
                        item1 = body.replace("%", val);
                        break;
                    case "time":
                        item2 = date.replace("%", val);
                        break
                    case "id":
                        item3 = links.replace("%", val);
                        item3 = item3.replace("%", val);
                        item3 = item3.replace("%", val);
                }
            });
            var tweet = wrap.replace("%", item1 + "<div class='meta'>" + item2 + item3 + "</div>");
            tweets.push(tweet);
        });

        var strBuilder = "<ul class='twitter-orbit' data-orbit>";
        for(var i = 0; i < tweets.length; i++)
        {   
            strBuilder += "<li>" + tweets[i] + "</li>";
        }
        $("#twitter-feed").append(strBuilder + "</ul>");
        $(document).foundation({
            orbit: {
                timer_speed: 7000, // Sets the amount of time in milliseconds before transitioning a slide
                pause_on_hover: false, // Pauses on the current slide while hovering
                resume_on_mouseout: false, // If pause on hover is set to true, this setting resumes playback after mousing out of slide
                next_on_click: false, // Advance to next slide on click
            }
        });
    });       
}

function is_home() {
    var url = window.location.href.split("/");
    if($(".home")[0] !== undefined)
        return true;
    else if(url[url.length - 1] == "index.php" || url[url.length - 1] == "index.html" || url[url.length - 2] == "wordpress")
        return true;
    return false;
}