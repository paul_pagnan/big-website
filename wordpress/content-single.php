<?php
/**
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="event-header-bg" style="background: url(<?php echo get_post_meta($post->ID, 'event_bg_image', true); ?>) center center">
		<div class="overlay"></div>
		<br />
		<div class="text-center event-title">
			<?php if ( get_theme_mod( 'eighties_singular_header_image' ) !== 'featured_image' && has_post_thumbnail() ) : ?>
				<figure class="entry-image">
					<?php the_post_thumbnail( 'main-featured' ); ?>
				</figure><!-- .entry-image -->
			<?php endif; ?>

		</div>

		<div class="entry-header row">
			<?php if ( !has_post_thumbnail() ) : ?>
				<br />
				<?php the_title( '<h1>', '</h1>' ); ?>
				<br />
				<br />
			<?php endif; ?>
			<div class="small-12 medium-8 small-centered columns">
				<div class="row event-header">
					<div class="small-4 columns">
						<i class="fa fa-map-marker"></i> <br />
						<?php echo get_post_meta($post->ID, 'event_location', true); ?>
					</div>
					<div class="small-4 columns">
						<i class="fa fa-ticket"></i> <br />
						<b>Member's Price: </b><?php echo get_post_meta($post->ID, 'event_members_price', true); ?><br />
						<b>Guest's Price: </b><?php echo get_post_meta($post->ID, 'event_guests_price', true); ?>
					</div>
					<div class="small-4 columns">
						<i class="fa fa-calendar-o"></i> <br />
						<?php echo get_post_meta($post->ID, 'event_day', true) ?>, <?php echo get_post_meta($post->ID, 'event_date', true) ?> <br />
						<?php echo get_post_meta($post->ID, 'event_time', true) ?>
					</div>
				</div>
		        <div class="row text-center">
		        	<a class="button radius outline medium" target="_blank" href="<?php echo get_post_meta($post->ID, 'event_fb_link', true); ?>" style="margin-right: 50px;"><i class="fa fa-facebook"></i> Facebook Event</a>
		        	<a class="button radius outline medium" target="_blank" href="<?php echo get_post_meta($post->ID, 'event_tickets_link', true); ?>"><i class="fa fa-ticket"></i> Get Tickets</a>
				</div>
			</div>
		</div><!-- .entry-header -->
	</div>
	<div class="row body-content">
		<div class="small-12">
			<div class="event-content">
				<?php the_title( '<h1 class="entry-title text-center">', '</h1>' ); ?>
				<br />
				<br />
				<?php the_content(); ?>
			</div><!-- .entry-content -->
		</div>
	</div>
	<?php the_tags( '<footer class="entry-footer"><div class="entry-meta entry-meta-tags">', ' ', '</div></footer><!-- .entry-footer -->' ); ?>
</article><!-- #post-## -->
