<?php
/**
 * eighties functions and definitions
 *
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 760; /* pixels */
}

/**
 * Eighties only works in WordPress 3.6 or later.
 * Let's make sure users do not run into any
 * troubles when trying to activate Eighties on WP
 * versions less than 3.6.
 *
 * @since 1.0.0
*/
if ( version_compare( $GLOBALS['wp_version'], '3.6', '<' ) ) {
	require_once( dirname( __FILE__ ) . '/inc/compatibility.php' );
}

/**
 * Set the theme mods in a global variable. This makes it easier to
 * retrieve in templates and functions.
 */
// $GLOBALS['eighties_theme_mod'] = get_theme_mod( 'eighties' );

if ( ! function_exists( 'eighties_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function eighties_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on eighties, use a find and replace
	 * to change 'eighties' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'eighties', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'main-featured', '1000', '700', array( 'center', 'center' ) );
	add_image_size( 'portfolio-featured', '500', '350', true );

	// This theme uses wp_nav_menu() in one location.
	// register_nav_menus( array(
	// 	'primary' => __( 'Primary Menu', 'eighties' ),
	// 	'social'  => __( 'Social Menu', 'eighties' )
	// ) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'gallery', 'image', 'video', 'audio', 'status', 'aside', 'link' ) );

	// Enable support for HTML5 markup.
	// add_theme_support( 'html5', array(
	// 	'comment-list',
	// 	// 'search-form',
	// 	'comment-form',
	// 	'gallery',
	// ) );

	// Add Editor Style
	add_editor_style( 'css/editor.css' );

	// Add shortcodes for the_excerpt
	add_filter( 'the_excerpt', 'do_shortcode' );
}
endif; // eighties_setup
add_action( 'after_setup_theme', 'eighties_setup' );

/**
 * Register a footer and interactive widget area.
 */
function eighties_widgets_init() {
	/**
	 * Set up our interactive sidebar if a user decided
	 * to enable this sidebar via the Customizer.
	*/
	register_sidebar( array(
		'name'          => __( 'Interactive Sidebar', 'eighties' ),
		'id'            => 'eighties-interactive-sidebar',
		'description'   => __( 'This sidebar opens as a toggle on the right side of a users browser window. Note that the toggle feature requires JavaScript in order to function. But no need to worry, a plain sidebar will appear if JavaScript does not work. If empty, the sidebar with not display.', 'eighties' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	// Footer widget area.
	register_sidebar( array(
		'name'          => __( 'Footer', 'eighties' ),
		'id'            => 'eighties-footer',
		'description'   => __( 'Widget area for the footer. If no widgets are provided, this footer will not appear.', 'eighties' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'eighties_widgets_init' );

/**
 * Register Righteous Google font for Eighties
 *
 * @since 1.0.0
 * @return string
*/
function eighties_header_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language
	 * that are not supported by Righteous, translate this to
	 * 'off'. Do not translate into your own language.
	*/
	if ( 'off' !== _x( 'on', 'Righteous font: on or off', 'eighties' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Righteous' ), "//fonts.googleapis.com/css" );
	}

	return $font_url;
}

/**
 * Register Raleway Google font for Eighties
 *
 * @since 1.0.0
 * @return string
*/
function eighties_headings_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language
	 * that are not supported by Raleway, translate this to
	 * 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Varela Round font: on or off', 'eighties' ) ) {
		$font_url = add_query_arg( 'family', 'Varela+Round', "//fonts.googleapis.com/css" );
	}

	return $font_url;
}

/**
 * Register Open Sans Google font for Eighties
 *
 * @since 1.0.0
 * @return string
*/
function eighties_body_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language
	 * that are not supported by Open Sans, translate this to
	 * 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'eighties' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Open Sans:400italic,700italic,400,700' ), "//fonts.googleapis.com/css" );
	}

	return $font_url;
}
/**
 * Enqueue scripts and styles.
 */
function eighties_styles() {
	$suffix = defined( 'STYLES_DEBUG' ) && STYLES_DEBUG ? '.css' : '.min.css';

	// Font Awesome Icons
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/fonts/fa/font-awesome' . $suffix );

	// Eighties Styles
	wp_enqueue_style( 'big', get_template_directory_uri() . '/library/stylesheets/app.css' );
 	wp_enqueue_style('googleFonts', 'http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic');

 
}
add_action( 'wp_enqueue_scripts', 'eighties_styles' );

/**
 * Enqueue scripts.
 */
function eighties_scripts() {

	// wp_register_script( 'jqueryBig', get_template_directory_uri() . '/library/bower_components/jquery/dist/jquery.min.js' );
	wp_enqueue_script( 'jqueryBig', get_template_directory_uri() . '/library/bower_components/jquery/dist/jquery.min.js', null, '1.0.0', true );
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/library/bower_components/foundation/js/foundation.min.js', null, '1.0.0', true );
	wp_enqueue_script( 'inta', get_template_directory_uri() . '/library/bower_components/instafeed.js/instafeed.min.js', null, '1.0.0', true );
	wp_enqueue_script( 'fastclick', get_template_directory_uri() . '/library/bower_components/fastclick/lib/fastclick.js', null, '1.0.0', true );
	wp_enqueue_script( 'countUp', get_template_directory_uri() . '/library/bower_components/countUp.js/countUp.min.js', null, '1.0.0', true );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/library/bower_components/modernizr/modernizr.js', null, '1.0.0', true );
	wp_enqueue_script( 'carousel', get_template_directory_uri() . '/library/bower_components/owl-carousel/owl.carousel.min.js', null, '1.0.0', true );
	wp_enqueue_script( 'skrollr', get_template_directory_uri() . '/library/bower_components/skrollr/dist/skrollr.min.js', null, '1.0.0', true );
	wp_enqueue_script( 'skrollr-menu', get_template_directory_uri() . '/library/bower_components/skrollr-menu/dist/skrollr.menu.min.js', null, '1.0.0', true );
	wp_enqueue_script( 'scrollReveal', get_template_directory_uri() . '/library/bower_components/scrollReveal.js/dist/scrollReveal.min.js', null, '1.0.0', true );
	wp_enqueue_script( 'big', get_template_directory_uri() . '/library/js/app.js', array('jqueryBig'), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'eighties_scripts' );

/**
 * Enqueue Header Google fonts style to admin
 * screen for custom header display.
 *
 * @since 1.0.0
 */
function eighties_admin_fonts() {
 	wp_enqueue_style('googleFonts', 'http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic');
}
add_action( 'admin_print_scripts-appearance_page_custom-header', 'eighties_admin_fonts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom post format functionality.
 */
require get_template_directory() . '/inc/post-formats.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/** UTS BiG Custom Functions **/
/**
 * UTS BiG - Custom Functions
 *
*/

// Page builder
function utsbig_get_sections($page_sections){
	if (!is_array($page_sections))
		return array();
	foreach ($page_sections as $sectionName){
		$q = new WP_Query('pagename='.$sectionName);
		if ($q->have_posts()){
			$q->the_post();
			?>
			<section id="<?php echo 'section-'.$q->post_name ?>" class="page-section">
				<h1><?php the_title()?></h1>
				<?php the_content() ?>
			</section>
			<?php
		}	
		wp_reset_postdata();
	}	
	return array();
	
}

// Home page banners info retrieval
function get_home_banners(){
	$dirName = 'img/banner/';
	$files = scandir(get_stylesheet_directory().'/'.$dirName);
	$dotFiles = array('.','..');
	$return = array();
	foreach ($files as $file){
		if (!in_array($file, $dotFiles)  && strrchr($file, '.') === '.jpg'){
			$return[] = $dirName.$file;
		}
	}
	return $return;
}

function utsbig_sponsors_image_uri(){
	return get_stylesheet_directory_uri().'/img/sponsors.jpg';
}
function utsbig_email_address(){
	return 'info@utsbig.com.au';
}
function utsbig_facebook_link(){
	return 'https://facebook.com/uts.big';
}

function utsbig_date($date_string, $date_format = 'd M Y'){
	return date($date_format, strtotime($date_string));
}
function utsbig_date_day($date_string){
	return utsbig_date($date_string, 'd');
}
function utsbig_date_month($date_string){
	return utsbig_date($date_string, 'M');
}
function utsbig_date_year($date_string){
	return utsbig_date($date_string, 'Y');
}	

function eventdate($attrs, $content = null) {
	$date_string = $content;
	$date = date('D, F j', strtotime($date_string));
	return $date;
}

add_shortcode( 'event-date', 'eventdate' );

// function description_careers_events(){
	// return apply_filters( 'the_content', '
	// <h2>About our Careers Events</h2>
	// ');
// }
// function description_social_events(){
	// return apply_filters( 'the_content', '
	// <h2>About our Social Events</h2>
	// ');	
// }

/** Wrapper that adds the actual metabox */
function utsbig_add_eventcustom_metabox(){
	add_meta_box(
		'utsbig_eventcustom_metabox',
		'Add Event Information (required)',
		'utsbig_print_eventcustom_metabox',
		'post'
	);
}
add_action('add_meta_boxes', 'utsbig_add_eventcustom_metabox');

/** Outputs the metabox */
function utsbig_print_eventcustom_metabox( $post ){
	wp_nonce_field( 'utsbig_print_eventcustom_metabox', 'utsbig_eventcustom_metabox_nonce' );
	
	$event_custom_list = utsbig_eventcustom_fields();
	echo '<table>';
	foreach ($event_custom_list as $event_custom_field){
		$event_custom_field_name = $event_custom_field[0];
		$event_custom_field_title = $event_custom_field[1];
		$event_custom_field_description = $event_custom_field[2];
		$value = get_post_meta( $post->ID, $event_custom_field_name, true );
		echo '<tr>';
		echo '<td><label for="eventcustom_'.$event_custom_field_name.'">';
		echo $event_custom_field_title;
		echo '</label></td>';
		echo '<td><input type="text" id="eventcustom_'.$event_custom_field_name.'" name="'.$event_custom_field_name.'" value="'.esc_attr($value).'" /></td>';
		echo '<td style="color: #aaa;">'.$event_custom_field_description.'</td>';
		echo '</tr>';
	}
	echo '</table>';
}

/** Save the metabox */
function utsbig_save_eventcustom_metabox( $post_id ){
	// Begin with lots of filtering
	// 1. Check if our nonce is set.
	if ( ! isset( $_POST['utsbig_eventcustom_metabox_nonce'] ) )
		return $post_id;
	// 2. Verify that the nonce is valid.
	$nonce = $_POST['utsbig_eventcustom_metabox_nonce'];	
	if ( ! wp_verify_nonce( $nonce, 'utsbig_print_eventcustom_metabox' ) )
		return $post_id;
	// 3. If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
	return $post_id;
	// 4. Check the user's permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) )
		return $post_id;

	/* OK, its safe for us to save the data now. */
	$event_custom_list = utsbig_eventcustom_fields();

	foreach ($event_custom_list as $event_custom){
		// Sanitize user input.		
		$mydata = sanitize_text_field( $_POST[$event_custom[0]] );
		// Update the meta field in the database.
		update_post_meta( $post_id, $event_custom[0], $mydata );
	}
}
add_action( 'save_post', 'utsbig_save_eventcustom_metabox' );



function utsbig_events_by_category($categoryID, $numPosts = -1, $past = false){
	if (is_array($categoryID)){
		$category_list = $category_ID;
	}
	else {
		$category_list = array($categoryID);
	}
	
	$comparison_operator = $past ? '<' : '>=';
	$order_direction = $past ? 'desc' : 'asc';
	
	return new WP_Query(array(
			'category__in' => $category_list,
			'posts' => $numPosts,
			'meta_key' => 'event_date',
			'orderby' => 'meta_value_num',
			'order' => $order_direction,
			'meta_query' => array(
				array(
					'key' => 'event_date',
					'value' => date('Ymd'),
					'type' => 'date',
					'compare' => $comparison_operator
				)
			),
		));
}

/** Event Custom fields */
function utsbig_eventcustom_fields(){
	return array(
		array('event_subtitle', 'Witty Subtitle'), 
		array('event_date', 'Date', 'Format example: "May 16"'), 
		array('event_day', 'Day', 'Format example: "Fri"'), 
		array('event_time', 'Time', 'Format example: "6PM"'), 
		array('event_fb_link', 'Facebook Link', 'Paste full event link here'), 
		array('event_location', 'Location Description'), 
		array('event_members_price', 'Members Price', 'Format $XX.XX'),
		array('event_guests_price', 'Guests Price', 'Format $XX.XX'),
		array('event_tickets_link', 'Tickets Link', 'Paste full event link here'),
		array('event_bg_image', 'Event Background Image', 'Please paste the link to the event background image')
	);
}

// Events custom metadata handler
function utsbig_event_custom($post_custom, $post_custom_keys){
	$subtitle = in_array('event_subtitle', $post_custom_keys) ? 
		reset($post_custom['event_subtitle']) : '';
	$location = in_array('event_location', $post_custom_keys) ? 
		reset($post_custom['event_location']) : '';
	$date = in_array('event_date', $post_custom_keys) ? 
		reset($post_custom['event_date']) : '';
	$day = in_array('event_day', $post_custom_keys) ? 
		reset($post_custom['event_day']) : '';
	$time = in_array('event_time', $post_custom_keys) ? 
		reset($post_custom['event_time']) : '';
	$members_price = in_array('event_members_price', $post_custom_keys) ? 
		reset($post_custom['event_members_price']) : '';
	$guests_price = in_array('event_guests_price', $post_custom_keys) ? 
		reset($post_custom['event_guests_price']) : '';
	$tickets_link = in_array('event_tickets_link', $post_custom_keys) ? 
		reset($post_custom['event_tickets_link']) : '';
	$fb_link = in_array('event_fb_link', $post_custom_keys) ? 
		reset($post_custom['event_fb_link']) : '';
	$bg_image = in_array('event_bg_image', $post_custom_keys) ? 
		reset($post_custom['event_bg_image']) : '';
		
		
	return ( object ) array(
		'index' => ( object ) array(
			'subtitle' => 'Subtitle',
			'location' => 'Location',
			'day' => 'day',
			'date' => 'Date',
			'time' => 'Time',
			'members_price' => 'Members\' Price',
			'guests_price' => 'Guests\' Price',
			'tickets_link' => 'Tickets',
			'bg_image' => 'Background Image',
		),
		'description' => ( object ) array(
			'subtitle' => '(required) Witty subtitle here',
			'location' => '(required) Location Description',
			'day' => '(Required) Format: Mon, Tue, Wed ...',
			'date' => '(Required) Format Month + Day',
			'time' => '(Required) Time',
			'members_price' => 'Format $XX.XX',
			'guests_price' => 'Format $XX.XX',
			'tickets_link' => 'Paste full event link here',			
			'fb_link' => 'Paste full event link here',			
			'bg_image' => 'Please paste the background image url',			
		),
		'subtitle' => $subtitle,
		'location' => $location,
		'day' => $day,
		'date' => $date,
		'time' => $time,
		'members_price' => $members_price,
		'guests_price' => $guests_price,
		'tickets_link' => $tickets_link,
		'fb_link' => $fb_link,
		'bg_image' => $bg_image,
	);
}


/* Random admin-menu meta related things */

// Remove post formats support
function remove_post_formats() {
    remove_theme_support('post-formats');
}
add_action('after_setup_theme', 'remove_post_formats', 11);

function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Events';
    $submenu['edit.php'][5][0] = 'Events';
    $submenu['edit.php'][10][0] = 'Add Event';
    // $submenu['edit.php'][15][0] = 'Status'; // Change name for categories
    // $submenu['edit.php'][16][0] = 'Labels'; // Change name for tags
    echo '';
}
function change_post_object_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = 'Events';
        $labels->singular_name = 'Event';
        $labels->add_new = 'Add Event';
        $labels->add_new_item = 'Add Event';
        $labels->edit_item = 'Edit Events';
        $labels->new_item = 'Event';
        $labels->view_item = 'View Event';
        $labels->search_items = 'Search Event';
        $labels->not_found = 'No Events found';
        $labels->not_found_in_trash = 'No Events found in Trash';
    }
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );


function append_category_name( $name ){
	return $name.' Archives';
}
add_filter( 'single_cat_title', 'append_category_name' );


add_theme_support('nav-menus');
if (function_exists('register_nav_menus')) {
    register_nav_menus( 
        array( 
            'main' => 'Main Nav'
        )
    );
}


class GC_walker_nav_menu extends Walker_Nav_Menu {
    // add classes to ul sub-menus
    function start_lvl(&$output, $depth) {
        // depth dependent classes
        $indent = ( $depth > 0 ? str_repeat("\t", $depth) : '' ); // code indent

        // build html
        $output .= "\n" . $indent . '<ul class="dropdown">' . "\n";
    }
}

if (!function_exists('GC_menu_set_dropdown')) :
function GC_menu_set_dropdown($sorted_menu_items, $args) {
    $last_top = 0;
    foreach ($sorted_menu_items as $key => $obj) {
        // it is a top lv item?
        if (0 == $obj->menu_item_parent) {
            // set the key of the parent
            $last_top = $key;
        } else {
            $sorted_menu_items[$last_top]->classes['dropdown'] = 'has-dropdown';
        }
    }

    return $sorted_menu_items;
}
endif;
add_filter('wp_nav_menu_objects', 'GC_menu_set_dropdown', 10, 2);

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
    if( in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

function big_menu() {
    $fixed = 'fixed';
    $spacer = "";
    $skrollr = "";
    if(is_front_page()) {
        $fixed='sticky';
        $skrollr = "data-0p='opacity:0;' data-25p='opacity:1;'";
   		$spacer = "<div class='spacer hide-for-small'></div>";
    }

    echo $spacer . "<div class='contain-to-grid " . $fixed . "'><nav class='top-bar' data-topbar role='navigation'><ul class='title-area'><li class='show-for-small'></li><li class='name'><a href='/' " . $skrollr . "><img class='logo' src='" .  get_template_directory_uri() . "/library/images/logo.png' /></a></li><li class='toggle-topbar menu-icon'><a href='#'><span>Menu</span></a></li></ul><section class='top-bar-section'>";
    $options = array(
        'menu' => 'Main',
        'theme_location' => 'primary',
        'container' => false,
        'depth' => 2,
        'items_wrap' => '<ul id="%1$s" class="right">%3$s',
        'walker' => new GC_walker_nav_menu()
    );
    wp_nav_menu($options);
    echo "<li><a class='button tiny radius outline' href='/join'>Join</a></ul></section></nav></div>";
}


add_shortcode( 'bigmenu', 'big_menu' );
