<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
 */

get_header(); ?>


	<?php if (is_front_page()) : ?>
		<div class="hero" id="top" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -75px;">
	<?php endif; ?>
	<?php big_menu(); ?>

	<?php if (!is_front_page()) : ?>
	<div class="row body-content">
		<div class="small-12">
	<?php endif; ?>


	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

		<?php
			// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || '0' != get_comments_number() ) :
				comments_template();
			endif;
		?>

	<?php endwhile; // end of the loop. ?>

	<?php if (!is_front_page()) : ?>
			</div>
		</div>
	<?php endif; ?>

<?php get_footer(); ?>
